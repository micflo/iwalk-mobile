package com.example.androidtimetable.api

import com.example.androidtimetable.model.Tour
import retrofit2.http.GET
import retrofit2.http.Query

interface RouteApi {

    @GET("bus-stop/routes?")
    suspend fun getRoutes(
            @Query("startLng") startLng: Double,
            @Query("startLat") startLat: Double,
            @Query("endLng") endLng: Double,
            @Query("endLat") endLat: Double
    ): List<Tour>
}