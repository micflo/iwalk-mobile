package com.example.androidtimetable.api

import android.util.Log
import com.example.androidtimetable.utils.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private val retrofit by lazy {
        Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    val api: RouteApi by lazy {
        Log.d("Retrofit", retrofit.baseUrl().toUri().toString())
        retrofit.create(RouteApi::class.java)
    }
}