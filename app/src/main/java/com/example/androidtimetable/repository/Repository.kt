package com.example.androidtimetable.repository

import com.example.androidtimetable.api.RetrofitInstance
import com.example.androidtimetable.model.Tour
import com.google.android.gms.maps.model.LatLng

class Repository {

    suspend fun getRoutes(start: LatLng, end: LatLng): List<Tour> {
        return RetrofitInstance.api.getRoutes(
                start.longitude,
                start.latitude,
                end.longitude,
                end.latitude)
    }
}