package com.example.androidtimetable.utils

class Constants {
    companion object {
        const val BASE_URL = "http://10.0.2.2:8080/api/v1/"
        const val DEFAULT_ZOOM = 15
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        const val KEY_CAMERA_POSITION = "camera_position"
        const val KEY_LOCATION = "location"
    }
}