package com.example.androidtimetable.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.androidtimetable.repository.Repository
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    val response: MutableLiveData<List<Tour>> = MutableLiveData()

    fun getRoutes(start: LatLng, end: LatLng) {
        viewModelScope.launch {
            response.value = Repository().getRoutes(start, end)
        }
    }
}