package com.example.androidtimetable.model

data class Leg(val steps: List<Step>)
