package com.example.androidtimetable.model

data class Step(
        val distance: Float,
        val duration: Float,
        val geometry: Geometry
)
