package com.example.androidtimetable.model

data class Route (
    val distance: Float,
    val duration: Float,
    val legs: List<Leg>
)