package com.example.androidtimetable.model

data class Geometry(
        val coordinates: List<List<Double>>
)
