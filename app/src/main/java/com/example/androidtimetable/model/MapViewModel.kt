package com.example.androidtimetable.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng

class MapViewModel : ViewModel() {

    private val _startLatLng = MutableLiveData<LatLng>()
    val startLatLng: LiveData<LatLng> = _startLatLng

    private val _endLatLng = MutableLiveData<LatLng>()
    val endLatLng: LiveData<LatLng> = _endLatLng


    fun setStartLatLng(startLatLng: LatLng) {
        _startLatLng.value = startLatLng
    }

    fun setEndLatLng(endLatLng: LatLng) {
        _endLatLng.value = endLatLng
    }
}