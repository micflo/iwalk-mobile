package com.example.androidtimetable.model

data class RequestSearch(
        val startLng: Double,
        val startLat: Double,
        val endLng: Double,
        val endLat: Double
)
