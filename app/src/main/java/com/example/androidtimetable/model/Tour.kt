package com.example.androidtimetable.model

data class Tour (
    val code: String,
    val routes: List<Route>
)