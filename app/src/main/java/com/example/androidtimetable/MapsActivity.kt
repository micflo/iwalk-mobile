package com.example.androidtimetable

import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.provider.SettingsSlicesContract.KEY_LOCATION
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.example.androidtimetable.model.MainViewModel
import com.example.androidtimetable.model.MapViewModel
import com.example.androidtimetable.model.Tour
import com.example.androidtimetable.utils.Constants.Companion.DEFAULT_ZOOM
import com.example.androidtimetable.utils.Constants.Companion.KEY_CAMERA_POSITION
import com.example.androidtimetable.utils.Constants.Companion.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener


const val TAG = "MapsActivity"

const val TOUR_ON_FOOT = 1
const val TOUR_VIA_BUS_STOPS = 2

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnPolylineClickListener {

    private lateinit var mMap: GoogleMap
    private val viewModel: MainViewModel by viewModels()
    private val mapViewModel: MapViewModel by viewModels()
    private var startMarker: Marker? = null
    private var polyline: Polyline? = null
    private var route = mutableListOf<LatLng>()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var startBusStopMarker: Marker? = null
    private var endBusStopMarker: Marker? = null
    private var polylineOnFoot: Polyline? = null
    private var polylineOnFootToStartBusStop: Polyline? = null
    private var polylineOnFootToEndBusStop: Polyline? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewModel.response.observe(this, Observer { response ->
            drawResponseOnMap(response)
        })

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        initLocationCallback()

        locationRequest = LocationRequest.create().apply {
            interval = 4000
            fastestInterval = 2000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val autocompleteFragment =
                supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                        as AutocompleteSupportFragment

        autocompleteFragment.setTypeFilter(TypeFilter.ESTABLISHMENT)
        autocompleteFragment.setCountry("PL")

        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        autocompleteFragment.setPlaceFields(mutableListOf(Place.Field.LAT_LNG))

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                place.latLng?.let {
                    mapViewModel.setEndLatLng(it)
                }
            }

            override fun onError(status: Status) {
                Log.i(TAG, "An error occurred: $status")
            }
        })
    }

    private fun drawResponseOnMap(tours: List<Tour>) {
        clearMap()
        when (tours.size) {
            TOUR_ON_FOOT -> {
                polylineOnFoot = mMap.addPolyline(
                    PolylineOptions()
                        .clickable(false)
                        .addAll(getRoute(tours[0]))
                )
            }
            TOUR_VIA_BUS_STOPS -> {
                val routeToStartBusStop = getRoute(tours[0])

                startBusStopMarker = mMap.addMarker(
                    MarkerOptions()
                        .icon(mapDrawableToBitmapDescriptor(R.drawable.ic_green_bus_24))
                        .position(routeToStartBusStop.last())
                )

                polylineOnFootToStartBusStop = mMap.addPolyline(
                    PolylineOptions()
                        .clickable(false)
                        .addAll(routeToStartBusStop)
                )

                val routeToEndBusStop = getRoute(tours[1])

                endBusStopMarker = mMap.addMarker(
                    MarkerOptions()
                        .icon(mapDrawableToBitmapDescriptor(R.drawable.ic_red_bus_24))
                        .position(routeToEndBusStop.first())
                )
                polylineOnFootToEndBusStop = mMap.addPolyline(
                    PolylineOptions()
                        .clickable(false)
                        .addAll(routeToEndBusStop)
                )
            }
        }
    }

    private fun clearMap() {
        polylineOnFoot?.remove()
        polylineOnFootToStartBusStop?.remove()
        polylineOnFootToEndBusStop?.remove()
        startBusStopMarker?.remove()
        endBusStopMarker?.remove()
    }
    private fun mapDrawableToBitmapDescriptor(drawableId: Int): BitmapDescriptor? {
        val drawable = ContextCompat.getDrawable(applicationContext, drawableId)!!
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun getRoute(tour: Tour): List<LatLng> {
        route = mutableListOf()
        tour.routes[0].legs[0].steps.forEach { it ->
            it.geometry.coordinates.forEach {
                route.add(LatLng(it[1], it[0]))
            }
        }
        return route;
    }

    private fun initLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    Log.d(TAG, "onLocationResult: $location")
                    val latLng = LatLng(location.latitude, location.longitude)
                    mapViewModel.setStartLatLng(latLng)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mMap.let { map ->
            outState.putParcelable(KEY_CAMERA_POSITION, map.cameraPosition)
            outState.putParcelable(KEY_LOCATION, lastKnownLocation)
        }
        super.onSaveInstanceState(outState)
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapClickListener {
            mapViewModel.setEndLatLng(it)
        }
        observeLocations()
        googleMap.setOnPolylineClickListener(this)

        getLocationPermission()

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI()

        // Get the current location of the device and set the position of the map.
        getDeviceLocation()
    }



    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnSuccessListener(this) { location ->
                    lastKnownLocation = location
                    if (lastKnownLocation != null) {
                        Log.d(TAG, "user location move")
                        val latLng = LatLng(
                            lastKnownLocation!!.latitude,
                            lastKnownLocation!!.longitude
                        )

                        mapViewModel.setStartLatLng(latLng)
                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                latLng, DEFAULT_ZOOM.toFloat()
                            )
                        )
                    }
                    locationResult.addOnFailureListener(this) { exception ->
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", exception)
                        mMap.moveCamera(
                            CameraUpdateFactory
                                .newLatLngZoom(LatLng(49.74079, 19.5905975), DEFAULT_ZOOM.toFloat())
                        )
                        mMap.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                    checkSettingsAndStartLocationUpdates()
                }
            }
        }
        updateLocationUI()
    }

    private fun checkSettingsAndStartLocationUpdates() {
        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build()
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder)

        task.addOnSuccessListener { locationSettingsResponse ->
            startLocationUpdates()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                try {
                    exception.startResolutionForResult(this@MapsActivity, 1001)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun observeLocations() {
        mapViewModel.startLatLng.observe(this, Observer {
            initStartAndEndPoint()
        })

        mapViewModel.endLatLng.observe(this, Observer {
            initStartAndEndPoint()
        })
    }

    private fun initStartAndEndPoint() {
        val endLatLng = mapViewModel.endLatLng.value
        val startLatLng = mapViewModel.startLatLng.value
        when {
            startLatLng != null && endLatLng != null -> {
                startMarker?.remove()
                startMarker = mMap.addMarker(MarkerOptions().position(endLatLng))
                viewModel.getRoutes(startLatLng, endLatLng)
            }
        }
    }

    override fun onPolylineClick(p0: Polyline?) {
        TODO("Not yet implemented")
    }
}